import React from 'react';
import { GlobalStyle } from '../src/theme';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { ThemeProvider } from 'styled-components';
import { themes } from '../src/theme';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
export const globalTypes = {
  theme: {
    name: 'THEME',
    description: 'Add theme...',
    defaultValue: 'light',
    toolbar: {
      icon: 'mirror',
      items: ['light', 'dark'],
      showName: true,
    },
  },
};

const withThemeProvider = (Story, context) => {
  return (
    <ThemeProvider theme={themes[context.globals.theme]}>
      <GlobalStyle />
      <Story {...context} />
    </ThemeProvider>
  );
};
export const decorators = [withThemeProvider];
