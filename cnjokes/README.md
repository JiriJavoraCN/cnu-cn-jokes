## Available Scripts

In the project directory, you can run:

### `yarn dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

### `yarn start`

Starts the built app from `yarn build`\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn eslint`

Check and fix all `.js` and `.jsx` files with eslint.

### `yarn prettier`

Format all `.js` and `.jsx` files with prettier.

### `yarn precommit`

Run `yarn eslint` and `yarn prettier` after each other.
