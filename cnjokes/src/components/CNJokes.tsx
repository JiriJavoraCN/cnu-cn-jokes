import React from 'react';
import { useRecoilValue } from 'recoil';
import { themeState } from './globalStateAtoms';
import { ThemeProvider } from 'styled-components';
import { GlobalStyle } from '../theme';

import { InteractiveSection } from './organisms';
import { Header } from './molecules';
import { Box, Background } from './atoms';

export function CNJokes(props: any) {
  const selectedTheme = useRecoilValue(themeState);
  return (
    <ThemeProvider theme={selectedTheme}>
      <GlobalStyle />
      <Background />
      <Box variant="appWrapper">
        <Box variant="mainContentWrapper">
          <Header text="CN JOKES" />
          <InteractiveSection />
          {props.children}
        </Box>
      </Box>
    </ThemeProvider>
  );
}
