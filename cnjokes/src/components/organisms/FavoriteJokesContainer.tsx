import React, { useEffect } from 'react';
import { Box, Heading, Text } from '../atoms';
import { getFavoriteJokesFromLocal, useIsMedium } from '../utils';
import { useRecoilState, useRecoilValue } from 'recoil';
import { favoriteJokesState, searchTextState } from '../globalStateAtoms';
import { JokesColumn } from '../molecules';

export function FavoriteJokesContainer() {
  const [savedFavoriteJokes, setSavedFavoriteJokes] = useRecoilState(favoriteJokesState);
  const searchText = useRecoilValue(searchTextState);
  const isMediumOrSmaller = useIsMedium();

  useEffect(() => {
    setSavedFavoriteJokes(getFavoriteJokesFromLocal());
  }, []);

  const filteredJokes = savedFavoriteJokes.filter((joke) =>
    joke.value.toLocaleLowerCase().includes(searchText.toLocaleLowerCase()),
  );
  const jokesToDisplay = searchText ? filteredJokes : savedFavoriteJokes;

  return (
    <Box
      oneOrTwoColumns={
        isMediumOrSmaller || savedFavoriteJokes.length < 2 || filteredJokes.length < 2
      }
      variant="favoriteJokesContainer"
    >
      <Heading variant="small">favorite jokes</Heading>
      {savedFavoriteJokes.length === 0 && (
        <Text variant="empty-message">{"You don't have any favorite jokes... "}</Text>
      )}
      {isMediumOrSmaller && <JokesColumn jokesToDisplay={savedFavoriteJokes} />}
      {!isMediumOrSmaller && (
        <>
          <JokesColumn
            jokesToDisplay={jokesToDisplay.filter((joke, index) => {
              return index % 2 === 0;
            })}
          />
          <JokesColumn
            jokesToDisplay={jokesToDisplay.filter((joke, index) => {
              return index % 2 === 1;
            })}
          />
        </>
      )}
    </Box>
  );
}
