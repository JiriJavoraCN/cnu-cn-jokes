import React, { useState } from 'react';
import { Box, Button, Text, TextArea, Heading, ToastMessage, List, ListElement } from '../atoms';
import { useRecoilState } from 'recoil';
import { addJokeTextState, categoriesState } from '../globalStateAtoms';
import { useMutation, gql } from '@apollo/client';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ADD_JOKE_MUTATION = gql`
  mutation addJoke($value: String!, $categories: [String]) {
    addCustomJoke(value: $value, categories: $categories) {
      value
      categories
    }
  }
`;

export function AddJokesSection() {
  const successMessage = () => toast.success('Joke successfully added!');
  const [showHint, setShowHint] = useState(false);
  const [addJokeText, setAddJokeText] = useRecoilState(addJokeTextState);
  const [{ selectedCategories }, setCategories] = useRecoilState(categoriesState);
  const [mutateFunction] = useMutation(ADD_JOKE_MUTATION, {
    variables: { value: addJokeText, categories: selectedCategories },
    onCompleted: () => {
      setAddJokeText('');
      setCategories((prevState) => {
        return { ...prevState, selectedCategories: [] };
      });
      successMessage();
    },
  });

  function handleSubmitJoke() {
    mutateFunction();
  }
  function toggleHint() {
    showHint ? setShowHint(false) : setShowHint(true);
  }
  return (
    <Box margin="1em 0 0 0" padding="1em">
      <Heading variant="small">ADD JOKE</Heading>
      <Box variant="hint">
        <Button variant="hint" onClick={toggleHint}>
          {showHint ? 'HIDE HINT' : 'SHOW HINT'}
        </Button>
      </Box>
      {showHint && (
        <List>
          <ListElement>1) add joke text</ListElement>
          <ListElement>
            2) (optional) select joke categories (from categories menu higher up)
          </ListElement>
          <ListElement>3) submit joke :)</ListElement>
        </List>
      )}

      <TextArea
        placeholder="This is a place to add some funny joke..."
        value={addJokeText}
        onInput={(event) => setAddJokeText(event.currentTarget.value)}
      />

      <Box variant="addJokeContainer">
        <Button variant="selected" width="max-content" padding="0.5em" onClick={handleSubmitJoke}>
          SUBMIT JOKE
        </Button>
        <ToastMessage />
      </Box>
    </Box>
  );
}
