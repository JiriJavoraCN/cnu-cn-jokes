import React from 'react';
import { Box } from '../../atoms';
import { JokesColumn } from '../../molecules/JokesColumn';
import { jokesState } from '../../globalStateAtoms';
import { useRecoilValue } from 'recoil';
import { useIsMedium } from '../../utils/hooks/useIsMedium';

export function JokesContainer() {
  const jokes = useRecoilValue(jokesState);
  const isMediumOrSmaller = useIsMedium();

  const oneOrTwoColumns = isMediumOrSmaller || jokes.length < 2;
  return (
    <Box oneOrTwoColumns={oneOrTwoColumns ?? true} variant="jokesContainer">
      {oneOrTwoColumns && <JokesColumn jokesToDisplay={jokes}></JokesColumn>}
      {!oneOrTwoColumns && (
        <>
          <JokesColumn
            jokesToDisplay={jokes.filter((joke, index) => {
              return index % 2 === 0;
            })}
          ></JokesColumn>
          <JokesColumn
            jokesToDisplay={jokes.filter((joke, index) => {
              return index % 2 === 1;
            })}
          ></JokesColumn>
        </>
      )}
    </Box>
  );
}
