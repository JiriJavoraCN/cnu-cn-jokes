import React from 'react';
import { JokesContainer } from '..';
import { Box } from '../../atoms';
import { JokeCard } from '../../molecules';

export default {
  title: 'molecules/JokesContainer',
  component: JokesContainer,
};

export const DefaultJokesContainer = (args) => {
  const showCards = () => {
    const cards: any = [];
    for (let i = 0; i < 10; i++) {
      cards.push(
        <JokeCard>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga velit aliquam consequuntur
          blanditiis neque dicta rem sint? Vero nemo incidunt corporis sint tempore officiis,
          temporibus maxime. Vel aperiam ab ut?
        </JokeCard>,
      );
    }
    return cards;
  };

  return (
    <Box padding="1em" display="grid">
      {showCards()}
    </Box>
  );
};
