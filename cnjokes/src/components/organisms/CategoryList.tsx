import React from 'react';
import { useRecoilState } from 'recoil';
import { Box, Button } from '../atoms';
import { categoriesState } from '../globalStateAtoms';
import { useQuery, gql } from '@apollo/client';

const CATEGORIES_QUERY = gql`
  query getCategories {
    getJokeCategories
  }
`;

export function CategoryList() {
  const [{ categories, selectedCategories }, setCategories] = useRecoilState(categoriesState);
  useQuery(CATEGORIES_QUERY, {
    onCompleted: (data) => {
      setCategories((prevState) => ({ ...prevState, categories: data.getJokeCategories }));
    },
  });

  const handleCategorySelect = (event: React.MouseEvent<HTMLButtonElement>) => {
    const { value } = event.currentTarget;
    setCategories((prevState) => {
      if (prevState.selectedCategories.includes(value)) {
        return {
          ...prevState,
          selectedCategories: selectedCategories.filter((category) => category !== value),
        };
      } else {
        return { ...prevState, selectedCategories: [...selectedCategories, value] };
      }
    });
  };

  return (
    <Box variant={'category-list'}>
      {categories.map((category, index) => (
        <Button
          variant={selectedCategories.includes(category) ? 'selected' : 'category'}
          key={index}
          value={category}
          onClick={handleCategorySelect}
        >
          {category.toUpperCase()}
        </Button>
      ))}
    </Box>
  );
}
