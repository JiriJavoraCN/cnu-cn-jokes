import React from 'react';
import styled from 'styled-components';
import { Box } from '../atoms';
import {
  ShowCategories,
  NumberOfJokes,
  ShowJokes,
  SearchJokeByText,
  FavoriteJokesButton,
  ModeSwitchButton,
  AddJokeButton,
} from '../molecules';
import { CategoryList } from '../organisms';
import { categoriesState } from '../globalStateAtoms';
import { useRecoilValue } from 'recoil';

import { mq } from '../../theme';

import { useRouter } from 'next/router';

export function InteractiveSection() {
  const { showCategories } = useRecoilValue(categoriesState);
  const router = useRouter();

  return (
    <Box variant="interactivesLayout">
      <SearchJokeByText />
      <FavoriteJokesButton />
      <ModeSwitchButton />
      <AddJokeButton />
      <ShowCategories />
      {showCategories && <CategoryList />}
      {router.pathname !== '/add-jokes' && (
        <Box variant="componentGroup">
          {(router.pathname === '/' || router.pathname === '/jokes') && <NumberOfJokes />}
          <ShowJokes />
        </Box>
      )}
    </Box>
  );
}
