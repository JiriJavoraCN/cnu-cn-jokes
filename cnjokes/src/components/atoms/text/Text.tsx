import styled, { css } from 'styled-components';
import { mq, Theme } from '../../../theme';

type TextVariants = 'plain-text' | 'empty-message' | 'hint';
type Props = {
  fontSize?: string;
  showLink?: boolean;
  variant?: string;
  theme?: Theme;
};

export const Text = styled.span<Props>`
  font-size: ${(props) => props.fontSize};
  padding: 2.5em 1em;
  border-radius: 1em;
  min-height: 7em;
  height: auto;
  background-color: transparent;
  word-wrap: break-word;
  white-space: wrap;
  overflow: hidden;
  display: flex;
  align-items: ${(props) => (props.showLink ? 'stretch' : 'center')};
  justify-content: center;
  ${({ variant }) => variants[variant ?? '']}
`;

const variants: { [key in TextVariants]: any } = {
  'plain-text': css`
    ${mq({
      fontSize: ['0.8em', '1em'],
    })}
  `,
  'empty-message': css`
    color: ${(props) => props.theme.fontColor};
    font-size: 1em;
    text-transform: uppercase;
    font-style: italic;
    grid-column: 1 / -1;
    text-align: center;
    ${mq({
      fontSize: ['0.8em', '1.4em'],
    })}
  `,
  hint: css`
    min-height: 1em;
    padding: 1em;
    font-style: italic;
    ${mq({
      fontSize: ['0.8em', '1em'],
    })}
  `,
};
