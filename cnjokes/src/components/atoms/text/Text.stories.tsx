import React from 'react';
import { Box, Text } from '..';

export default {
  title: 'atoms/Text',
  component: Text,
  argTypes: {
    fontSize: {
      control: { type: 'range', min: 1, max: 3, step: 0.2 },
      defaultValue: 1,
    },
  },
};

export const DefaultText = (args) => (
  <Box width="auto" padding="1em">
    <Text {...args} fontSize={`${args.fontSize}em`}>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas possimus sit ad fugit sapiente
      adipisci magni reprehenderit optio vero voluptatibus repellat ea, blanditiis reiciendis,
      eveniet cumque beatae ipsa nam debitis! Voluptatem accusantium nobis perspiciatis magnam,
      reiciendis nulla dolore earum ex optio eaque ratione odio deserunt quaerat eos quidem tempora
      dolorem iusto! Voluptate, nesciunt! Error itaque rerum possimus dolore maiores officiis!
      Quidem, nisi aliquam! Quaerat eligendi atque soluta. Deleniti, distinctio perspiciatis
      voluptas, maxime molestiae accusantium illum fuga laboriosam molestias ea cumque excepturi
      esse hic aspernatur in nihil, laudantium facilis rem quia? Dicta distinctio, labore incidunt
      id eius nihil illo harum molestiae asperiores consectetur. Exercitationem quia quibusdam
      deleniti vero omnis voluptatibus ut est dolore dignissimos pariatur cum officiis possimus, hic
      modi facilis? Suscipit nulla blanditiis expedita corrupti? Alias totam aut animi eius vitae
      perspiciatis, in tempore nihil? Similique saepe, ratione provident quo enim nemo corrupti
      aspernatur hic tempore, quibusdam illo at omnis?
    </Text>
  </Box>
);
