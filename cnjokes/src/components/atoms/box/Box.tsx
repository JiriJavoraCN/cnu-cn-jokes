import styled, { css } from 'styled-components';
import { mq } from '../../../theme';

type BoxVariant =
  | 'category-list'
  | 'jokesContainer'
  | 'favoriteJokesContainer'
  | 'show-jokes'
  | 'jokeCard'
  | 'mainContentWrapper'
  | 'appWrapper'
  | 'interactivesLayout'
  | 'addJokeContainer'
  | 'hint'
  | 'componentGroup';

type Props = {
  margin?: string;
  padding?: string;
  borderRadius?: string;
  display?: string;
  width?: string;
  height?: string;
  backgroundColor?: string;
  oneOrTwoColumns?: boolean;
  gridColumnStart?: string;
  gridColumnEnd?: string;
  minWidth?: string;
  visibility?: string;
  flex?: string;
  flexDirection?: string;
  hoveredColor?: string;
  variant?: BoxVariant;
  filter?: string;
  position?: string;
  justifyContent?: string;
};

export const Box = styled.div<Props>`
  margin: ${(props) => props.margin ?? '0'};
  padding: ${(props) => props.padding};
  border-radius: ${(props) => props.borderRadius ?? '1em'};
  display: ${(props) => props.display ?? 'block'};
  width: ${(props) => props.width ?? 'auto'};
  height: ${(props) => props.height ?? 'auto'};
  background-color: ${(props) => props.backgroundColor ?? props.theme.wrapperDivColor};
  gap: ${(props) => (props.oneOrTwoColumns ? '0em' : '1em')};
  grid-template-columns: ${(props) => (props.oneOrTwoColumns ? '1fr' : '1fr  1fr')};
  backdrop-filter: ${(props) => props.filter ?? 'blur(0.2em)'};
  color: ${(props) => props.theme.fontColor};
  grid-column: ${(props) => props.gridColumnStart ?? ''} / ${(props) => props.gridColumnEnd ?? ''};
  min-width: ${(props) => props.minWidth};
  visibility: ${(props) => props.visibility};
  flex: ${(props) => props.flex};
  flex-direction: ${(props) => props.flexDirection};
  justify-content: ${(props) => props.justifyContent};
  position: ${(props) => props.position};

  :hover {
    background: ${(props) => props.hoveredColor ?? undefined};
  }
  ${({ variant }) => (variant ? variants[variant] : '')}
`;

const variants: { [key in BoxVariant]: {} } = {
  'category-list': css`
    padding: 1em;
    width: 100%;
    display: grid;
    ${mq({
      order: ['0', '1'],
      gridTemplateColumns: ['repeat(3, 1fr)', 'repeat(3, 1fr)', 'repeat(5, 1fr)', 'repeat(8, 1fr)'],
      padding: ['0.5em', '0.8em', '1em'],
    })}
    grid-auto-flow: row dense;
    gap: 0.5em;
  `,
  jokesContainer: css`
    margin: 1em 0 0 0;
    display: grid;
    ${mq({
      padding: ['0.5em', '0.8em', '1em'],
    })}
  `,
  favoriteJokesContainer: css`
    margin: 1em 0 0 0;
    display: grid;
    ${mq({
      padding: ['0.5em', '0.8em', '1em'],
    })};
  `,
  'show-jokes': css`
    padding: 0;
    width: 100%;
    min-width: 8em;
  `,
  jokeCard: css`
    border-radius: 1em;
    background-color: ${(props) => props.theme.cardColor};
    color: ${(props) => props.theme.cardContentColor};
    position: relative;
    display: grid;
    flex-direction: column;
    align-items: center;
    padding: 0.5em;
  `,
  mainContentWrapper: css`
    background-color: transparent;
    backdrop-filter: none;
    height: 100%;
    margin: 0 auto;
    ${mq({
      width: ['100%', '90%', '80%', '70%'],
    })}
  `,
  appWrapper: css`
    background-color: transparent;
    backdrop-filter: none;
    min-height: 100vh;
    display: flex;
    justify-content: space-between;
    overflow: hidden;
    ${mq({
      padding: ['1.25em', '2em'],
    })}
  `,
  interactivesLayout: css`
    backdrop-filter: none;
    background-color: transparent;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    height: 100%;
    margin: 1em 0 0 0;
    ${mq({
      gap: ['1em 0.5em', '1em'],
    })}
  `,
  addJokeContainer: css`
    width: 100%;
    display: flex;
    justify-content: center;
    background-color: transparent;
    backdrop-filter: none;
  `,
  hint: css`
    padding: 0.5em;
    display: flex;
    justify-content: center;
    background-color: transparent;
    backdrop-filter: none;
  `,
  componentGroup: css`
    display: flex;
    flex: 1 1 30%;
    background-color: transparent;
    backdrop-filter: none;
    ${mq({
      gap: ['1em 0.5em', '1em'],
      flexBasis: ['50%', '30%'],
    })};
  `,
};
