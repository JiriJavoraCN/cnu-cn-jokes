import React from 'react';
import { Box } from './Box';
import { Story } from '@storybook/react';

export default {
  title: 'atoms/Box',
  component: Box,
  argTypes: {
    width: {
      control: { type: 'number', min: 0 },
      defaultValue: 100,
    },
    height: {
      control: { type: 'number', min: 0 },
      defaultValue: 100,
    },
  },
};

const Template: Story<any> = (args) => (
  <Box width={`${args.width}px`} height={`${args.height}px`} />
);

export const DefaultBox = Template.bind({});
DefaultBox.args = {
  label: 'Box',
};
