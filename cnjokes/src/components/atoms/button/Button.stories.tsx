import React from 'react';
import { Button, Box } from '..';

export default {
  title: 'atoms/Button',
  component: Button,
  argTypes: {
    variant: {
      options: ['selected', 'category', 'show-jokes'],
      control: { type: 'radio' },
      defaultValue: 'category',
    },
    label: {
      control: {
        type: 'text',
      },
      defaultValue: 'BUTTON',
    },
    padding: {
      control: { type: 'range', min: 1, max: 10 },
      defaultValue: 1,
    },
  },
};

export const ButtonsWithBox = (args) => {
  return (
    <Box width="max-content" padding={`${args.padding}em`}>
      <Button variant={args.variant} width={'auto'}>
        {args.label}
      </Button>
    </Box>
  );
};
