import styled, { css } from 'styled-components';
import { mq, colorPalette } from '../../../theme';

export type ButtonVariants =
  | 'selected'
  | 'category'
  | 'show-jokes'
  | 'categories'
  | 'icon-button'
  | 'hint';

type Props = {
  background?: string;
  border?: string;
  cursor?: string;
  padding?: string;
  width?: string;
  height?: string;
  hoveredColor?: string;
  variant?: ButtonVariants;
  right?: string;
};

export const Button = styled.button<Props>`
  background-color: ${(props) => props.background ?? 'transparent'};
  border: ${(props) => props.border ?? '0px solid transparent'};
  border-color: ${(props) => props.theme.fontColor ?? 'transparent'};
  outline: none;
  cursor: ${(props) => props.cursor ?? 'pointer'};
  padding: ${(props) => props.padding ?? '0.5em'};
  width: ${(props) => props.width ?? '100%'};
  height: ${(props) => props.height ?? '100%'};
  color: ${(props) => props.theme.fontColor ?? colorPalette.white};
  text-align: center;
  border-radius: 1em;
  ${mq({
    fontSize: ['0.8em', '1em'],
  })}

  right: ${(props) => props.right ?? '0.5em'};

  :hover {
    background: ${(props) => props.hoveredColor ?? colorPalette.focusedTextInputColor};
  }

  ${({ variant }) => (variant ? variants[variant] : '')}
`;

const variants: { [key in ButtonVariants]: {} } = {
  'show-jokes': css`
    background: ${(props) => props.theme.showJokesButtonColor};
    color: ${colorPalette.white};
    border-radius: 1em;
    vertical-align: middle;
    ${mq({
      fontSize: ['0.8em', '1em', '1.25em'],
    })}
    :hover {
      background: ${(props) => props.theme.showJokesButtonColorHovered};
    }
  `,
  selected: css`
    background-color: ${(props) => props.theme.optionSelectedColor};
    color: ${colorPalette.white};
    border: 1px solid ${(props) => props.theme.fontColor};
    ${mq({
      fontSize: ['0.8em', '1.25em'],
    })}
    :hover {
      background: ${(props) => props.theme.optionSelectedHoveredColor};
    }
  `,
  category: css`
    border: 1px solid ${(props) => props.theme.fontColor};
    ${mq({
      fontSize: ['0.7em', '0.9em', '1.1em'],
    })}
  `,
  categories: css`
    ${mq({
      fontSize: ['0.8em', '1em', '1.25em'],
    })}
  `,
  'icon-button': css`
    font-size: 1em;
    color: ${(props) => props.theme.cardContentColor};
    border: none;
    background-color: transparent;
    position: absolute;
    cursor: pointer;
    width: max-content;
    height: auto;
    padding: 0em;
    ${mq({
      top: ['0.5em', '1em'],
    })}
    :hover {
      background: transparent;
    }
  `,
  hint: css`

    background-color: transparent;
    width: max-content;
    font-style: italic;
    :hover {
      transition: ease 1s;
      text-decoration: underline;
      background-color: transparent;
    }
  `,
};
