import React from 'react';
import { Icon, Box } from '..';

export default {
  title: 'atoms/Icon',
  component: Icon,
  argTypes: {
    icon: {
      options: ['sun', 'moon', 'coffee', 'angry'],
      control: { type: 'radio' },
      defaultValue: 'sun',
    },
    fontSize: {
      control: { type: 'range', min: 1, max: 10 },
      defaultValue: 1,
    },
  },
};

export const DefaultIcon = (args) => (
  <Box width="max-content" padding="1em">
    <Icon className={`fas fa-${args.icon}`} fontSize={`${args.fontSize}em`} />
  </Box>
);
