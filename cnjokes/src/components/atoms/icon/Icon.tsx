import styled, { css } from 'styled-components';
import { mq } from '../../../theme';
type IconVariants = 'warning' | 'onCard' | 'small' | 'large' | 'tooltip';
type Props = {
  fontSize?: string;
  justifySelf?: string;
  margin?: string;
  padding?: string;
  variant?: string;
};

export const Icon = styled.i<Props>`
  align-self: center;
  font-size: ${(props) => props.fontSize ?? '1em'};
  justify-self: ${(props) => props.justifySelf ?? 'unset'};
  margin: ${(props) => props.margin ?? '0em'};
  padding: ${(props) => props.padding ?? '0em'};
  ${({ variant }) => variants[variant ?? '']}
`;

const variants: { [key in IconVariants]: {} } = {
  warning: css`
    color: ${(props) => props.theme.invalidInputColor};
    position: absolute;
    right: 1em;
  `,
  onCard: css`
    ${mq({
      fontSize: ['0.8em', '1em'],
    })}
    padding:0em;
    width: 1.2em;
  `,

  small: css`
    ${mq({
      fontSize: ['0.8em', '1em'],
    })}
    margin-left: 1em;
  `,

  large: css`
    ${mq({
      fontSize: ['0.9em', '1em', '1.5em'],
      width: ['1.9em', '2em', '2.5em'],
    })}
    padding: 0;
  `,
  tooltip: css`
      ${mq({
        fontSize: ['1em', '1.25em'],
      })};
  `,
};
