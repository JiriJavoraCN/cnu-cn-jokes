import React from 'react';
import { ToastContainer } from 'react-toastify';

export const ToastMessage = () => {
  return (
    <ToastContainer
      pauseOnHover={true}
      position="top-center"
      hideProgressBar={true}
      closeButton={false}
    />
  );
};
