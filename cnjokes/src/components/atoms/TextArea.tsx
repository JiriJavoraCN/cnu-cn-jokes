import styled from 'styled-components';
import { mq } from '../../theme';

export const TextArea = styled.textarea`
  width: 100%;
  min-height: 20vh;
  max-width: 100%;
  border-radius: 1em;
  outline: none;
  ${mq({
    fontSize: ['0.8em', '1.2em', '1.3em', '1.5em'],
  })}
  margin-bottom: 0.5em;
  resize: none;
  padding: 1em;
  ::placeholder {
    ${mq({
      fontSize: ['0.4em', '0.8em', '1.1em', '1.25em'],
    })}
    text-align: center;

  }
`;
