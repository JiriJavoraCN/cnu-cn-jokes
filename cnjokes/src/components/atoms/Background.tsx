import styled from 'styled-components';

export const Background = styled.div`
  position: fixed;
  z-index: -1;
  width: 100%;
  height: 100vh;
  background-image: url(/chuckNorris1.jpg);
  background-color: ${(props) => props.theme.backgroundColor};
  background-blend-mode: soft-light;
  background-size: cover;
  background-position: center;
`;
