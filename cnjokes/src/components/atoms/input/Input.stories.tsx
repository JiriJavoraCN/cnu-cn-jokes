import React from 'react';
import { Box, Input } from '..';

export default {
  title: 'atoms/Input',
  component: Input,
  argTypes: {
    size: {
      options: ['small', 'large'],
      control: { type: 'radio' },
      defaultValue: 'small',
    },
    placeholder: {
      control: { type: 'text' },
      defaultValue: 'Add some text...',
    },
  },
};

export const DefaultText = (args) => (
  <Box width="max-content" padding="1em">
    <Input variant={args.size} placeholder={args.placeholder} />
  </Box>
);
