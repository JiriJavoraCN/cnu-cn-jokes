import styled, { css } from 'styled-components';
import { mq } from '../../../theme';

type InputVariants = 'small' | 'large';
type Props = {
  fontSize?: string;
  variant?: string;
};

export const Input = styled.input<Props>`
  background: transparent;
  border: none;
  -moz-appearance: textfield;
  width: 100%;
  font-size: ${(props) => props.fontSize ?? '1em'};
  color: ${(props) => props.theme.fontColor};
  text-align: center;
  ${({ variant }) => variants[variant ?? '']}

  ::placeholder {
    font-style: italic;
    color: ${(props) => props.theme.placeholderTextColor};
    text-align: center;
    font-size: ${(props) => props.fontSize ?? '1em'};
    font-weight: 300;
  
  }

  :focus {
    outline: none;
    border: none;
  }

  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }
`;
const variants: { [key in InputVariants]: {} } = {
  small: css`
    ${mq({
      fontSize: ['0.8em', '1em', '1.1em', '1.25em'],
    })}
  `,
  large: css`
    ${mq({
      fontSize: ['0.8em', '1.5em', '2.25em'],
    })}
  `,
};
