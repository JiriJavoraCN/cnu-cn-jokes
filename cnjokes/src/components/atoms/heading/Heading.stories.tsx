import React from 'react';
import { Heading, Box } from '..';

export default {
  title: 'atoms/Heading',
  component: Heading,
  argTypes: {
    label: {
      control: {
        type: 'text',
      },
      defaultValue: 'THIS IS HEADING',
    },
    fontSize: {
      control: { type: 'range', min: 2, max: 8 },
      defaultValue: 1,
    },
  },
};

export const DefaultHeading = (args) => (
  <Box width="max-content" padding="1em">
    <Heading variant="story" fontSize={`${args.fontSize}em`}>
      {args.label}
    </Heading>
  </Box>
);
