import styled, { css } from 'styled-components';
import { mq } from '../../../theme';

type HeadingVariants = 'large' | 'small' | 'story';

type Props = {
  padding?: string;
  variant?: HeadingVariants;
  fontSize?: string;
};

export const Heading = styled.h1<Props>`
  color: ${(props) => props.theme.fontColor};
  margin: 0 auto;
  padding: ${(props) => props.padding};
  letter-spacing: 0.25em;
  text-indent: 0.25em;
  font-weight: 200;
  text-align: center;
  text-transform: uppercase;
  ${({ variant }) => (variant ? variants[variant] : '')}
`;

const variants: { [key in HeadingVariants]: {} } = {
  large: css`
    ${mq({
      fontSize: ['2em', '3em', '5em', '7em'],
      lineHeight: ['1.5em', '1.2em'],
    })};
  `,
  small: css`
    ${mq({
      fontSize: ['1.2em', '2em', '3em'],
      lineHeight: ['1.2em', '1em'],
    })};
    padding: 0.5em;
    grid-column: 1 / -1;
  `,
  story: css`
    font-size: ${(props: Props) => props.fontSize};
  `,
};
