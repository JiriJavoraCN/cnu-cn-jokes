import styled from 'styled-components';
import { mq } from '../../theme';

export const ListElement = styled.li`
  max-width: max-content;
  font-weight: 100;
  font-style: italic;
  line-height: 2em;
  ${mq({
    fontSize: ['0.6em', '1em'],
  })}
`;

export const List = styled.ul`
  margin: 1em 0 1em 0;
  list-style: none;
  padding: 0em 1em;

`;
