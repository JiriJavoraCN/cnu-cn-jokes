export const synth = typeof window !== 'undefined' ? window.speechSynthesis : null;

export function startSpeech(textToSpeech, startHandler, stopHandler) {
  if (synth) {
    const voices = synth.getVoices();
    const msg = new SpeechSynthesisUtterance(textToSpeech);
    msg.voice = voices.find((voice) => voice.lang === 'en-GB') ?? null;
    msg.lang = 'en-GB';
    msg.onstart = startHandler;
    msg.onend = stopHandler;
    synth.speak(msg);
  }
}

export function stopSpeech() {
  if (synth) {
    synth.cancel();
  }
}
