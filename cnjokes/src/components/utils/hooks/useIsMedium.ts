import { useMediaQuery } from 'react-responsive';

export function useIsMedium() {
  return useMediaQuery({ query: '(max-width: 920px)' });
}
