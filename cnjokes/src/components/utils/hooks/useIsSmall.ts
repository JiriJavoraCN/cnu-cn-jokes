import { useMediaQuery } from 'react-responsive';

export function useIsSmall() {
  return useMediaQuery({ query: '(max-width: 470px)' });
}
