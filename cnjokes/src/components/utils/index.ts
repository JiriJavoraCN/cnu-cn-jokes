export { getFavoriteJokesFromLocal } from './getFavoriteJokesFromLocal';
export { useIsMedium } from './hooks/useIsMedium';
export { startSpeech, stopSpeech } from './textToSpeech';
