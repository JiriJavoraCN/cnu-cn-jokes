export function getFavoriteJokesFromLocal() {
  return JSON.parse(localStorage.getItem('favoriteJokes') || '[]');
}
