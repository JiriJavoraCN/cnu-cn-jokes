import { atom } from 'recoil';
import { themes } from '../theme';

export type Joke = {
  categories: string[];
  created_at: string;
  icon_url: string;
  _id: string;
  updated_at: string;
  url: string;
  value: string;
};

const defaultJokesState: Joke[] = [];

export const jokesState = atom({
  key: 'jokes',
  default: defaultJokesState,
});

export const searchTextState = atom({
  key: 'searchText',
  default: '',
});

type CategoriesState = {
  categories: string[];
  showCategories: boolean;
  selectedCategories: string[];
};

const defaultCategoriesState: CategoriesState = {
  categories: [],
  showCategories: false,
  selectedCategories: [],
};

export const categoriesState = atom({
  key: 'categories',
  default: defaultCategoriesState,
});

export const themeState = atom({
  key: 'theme',
  default: themes.light,
});

export const numberOfJokesState = atom({
  key: 'numberOfJokes',
  default: 0,
});

export const showingFavoritesListState = atom({
  key: 'showingFavoritesList',
  default: false,
});

const defaultFavoriteJokesState: Joke[] = [];

export const favoriteJokesState = atom({
  key: 'favoriteJokes',
  default: defaultFavoriteJokesState,
});

export const showingAddJokesState = atom({
  key: 'addJokes',
  default: false,
});

export const addJokeTextState = atom({
  key: 'addJokeText',
  default: '',
});
