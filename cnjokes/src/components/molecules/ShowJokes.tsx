import React, { useEffect } from 'react';
import { Button } from '../atoms';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import {
  categoriesState,
  jokesState,
  searchTextState,
  numberOfJokesState,
} from '../globalStateAtoms';
import { useQuery, gql } from '@apollo/client';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useIsSmall } from '../utils/hooks/useIsSmall';

const RANDOM_JOKES_QUERY = gql`
  query getRandomJokes($numberOfJokes: Int!, $jokeCategories: [String]) {
    getRandomJokes(numOfJokes: $numberOfJokes, jokeCategories: $jokeCategories) {
      ... on joke {
        value
        _id
      }
    }
  }
`;

const SEARCH_JOKES_QUERY = gql`
  query searchJokesByText($text: String!, $jokeCategories: [String]) {
    getJokesByText(text: $text, jokeCategories: $jokeCategories) {
      ... on joke {
        categories
        value
        _id
      }
    }
  }
`;

export function ShowJokes() {
  const { selectedCategories } = useRecoilValue(categoriesState);
  const numberOfJokes = useRecoilValue(numberOfJokesState);
  const setJokes = useSetRecoilState(jokesState);
  const searchText = useRecoilValue(searchTextState);
  const router = useRouter();
  const isSmall = useIsSmall();

  const { refetch: refetchJokesByText } = useQuery(SEARCH_JOKES_QUERY, {
    variables: { text: searchText, jokeCategories: selectedCategories },
    skip: true,
    onCompleted: ({ getJokesByText: jokesByText }) => setJokes(jokesByText),
  });

  const { refetch: refetchRandomJokes } = useQuery(RANDOM_JOKES_QUERY, {
    variables: {
      numberOfJokes: numberOfJokes !== 0 ? numberOfJokes : 1,
      jokeCategories: selectedCategories,
    },
    onCompleted: ({ getRandomJokes: randomJokes }) => setJokes(randomJokes),
  });

  async function showJokesByText() {
    const {
      data: { getJokesByText: jokesByTextData },
    } = await refetchJokesByText({ text: searchText, jokeCategories: selectedCategories });
    setJokes(jokesByTextData);
  }

  async function showRandomJokes() {
    const {
      data: { getRandomJokes: randomJokes },
    } = await refetchRandomJokes({
      numberOfJokes: numberOfJokes !== 0 ? numberOfJokes : 1,
      jokeCategories: selectedCategories,
    });
    setJokes(randomJokes);
  }

  async function handleShowJokes() {
    if (searchText.length > 0) {
      showJokesByText();
    } else {
      showRandomJokes();
    }
  }

  useEffect(() => {
    if (router.pathname !== '/add-jokes') {
      handleShowJokes();
    }
  }, [selectedCategories, numberOfJokes, searchText]);

  return (
    <Link href="/jokes" passHref>
      <Button variant="show-jokes" onClick={() => handleShowJokes()}>
        {isSmall ? 'JOKES!' : 'SHOW ME JOKES!'}
      </Button>
    </Link>
  );
}
