import React from 'react';
import { IconButton } from '.';
import { useSetRecoilState } from 'recoil';
import { favoriteJokesState, Joke } from '../globalStateAtoms';

type Props = {
  isFavorite: boolean;
  joke: Joke;
};
export function AddToFavoritesButton(props: Props) {
  const setSavedFavoriteJokes = useSetRecoilState(favoriteJokesState);

  const addToFavorites = () => {
    if (!props.isFavorite) {
      setSavedFavoriteJokes((prevState) => {
        const result = [...prevState, props.joke];
        localStorage.setItem('favoriteJokes', JSON.stringify(result));
        return result;
      });
    } else {
      setSavedFavoriteJokes((prevState) => {
        const result = prevState.filter((joke) => props.joke._id !== joke._id);
        localStorage.setItem('favoriteJokes', JSON.stringify(result));
        return result;
      });
    }
  };

  return (
    <IconButton
      onClick={addToFavorites}
      iconVariant="onCard"
      buttonVariant="icon-button"
      className={`fa${props.isFavorite ? 's' : 'r'} fa-star`}
      boxBackground="transparent"
    />
  );
}
