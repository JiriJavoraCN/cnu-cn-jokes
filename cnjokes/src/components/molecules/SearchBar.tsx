import React from 'react';
import { Box, Input, Icon } from '../atoms';

export function SearchBar(props) {
  return (
    <Box padding="0" flex="1 1 50%" display={'flex'}>
      <Input
        variant={props.inputVariant}
        placeholder={props.placeholder}
        value={props.value}
        onInput={props.onInput}
        fontSize={props.fontSize}
      />
      <Icon className={props.className} padding={'0 1em 0 0'} variant="large" />
    </Box>
  );
}
