import React from 'react';
import { JokeCard } from '.';
import { showingFavoritesListState, favoriteJokesState, Joke } from '../globalStateAtoms';
import { useRecoilValue } from 'recoil';
import { Box } from '../atoms';

type Props = {
  jokesToDisplay: Joke[];
};
export function JokesColumn({ jokesToDisplay }: Props) {
  const savedFavoriteJokes = useRecoilValue(favoriteJokesState);
  return (
    <Box
      filter="none"
      backgroundColor="transparent"
      display="flex"
      flexDirection="column"
      visibility={showingFavoritesListState ? 'visible' : 'hidden'}
    >
      {jokesToDisplay &&
        jokesToDisplay.map((joke: Joke) => {
          return (
            <JokeCard
              key={joke._id}
              joke={joke}
              isFavorite={savedFavoriteJokes.some((item) => {
                return item._id === joke._id;
              })}
            >
              {joke.value}
            </JokeCard>
          );
        })}
    </Box>
  );
}
