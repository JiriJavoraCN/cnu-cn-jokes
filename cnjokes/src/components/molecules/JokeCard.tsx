import React from 'react';
import { TellJokeButton, AddToFavoritesButton } from './';
import { Box, Text } from '../atoms';
import { Joke } from '../globalStateAtoms';

type Props = {
  oneColumn?: boolean;
  joke?: Joke;
  isFavorite?: boolean;
  children?: string;
};

export function JokeCard(props: Props) {
  return (
    <Box
      gridColumnStart={props.oneColumn ? '1' : undefined}
      gridColumnEnd={props.oneColumn ? '-1' : undefined}
      variant="jokeCard"
      oneOrTwoColumns
    >
      <AddToFavoritesButton joke={props.joke} isFavorite={props.isFavorite} />
      <TellJokeButton joke={props.joke} />
      <Text variant="plain-text">{props.children}</Text>
    </Box>
  );
}
