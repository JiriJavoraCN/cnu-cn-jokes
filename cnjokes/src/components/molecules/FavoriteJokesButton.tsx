import React from 'react';
import { IconButton } from '.';
import Link from 'next/link';
import { useRouter } from 'next/router';

export function FavoriteJokesButton() {
  const router = useRouter();

  return (
    <Link href={router.pathname === '/favorite-jokes' ? '/' : '/favorite-jokes'} passHref>
      <IconButton
        className={`fa${router.pathname === '/favorite-jokes' ? 's' : 'r'} fa-star`}
        iconVariant="large"
      />
    </Link>
  );
}
