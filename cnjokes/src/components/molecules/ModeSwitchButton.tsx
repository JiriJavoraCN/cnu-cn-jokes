import React from 'react';
import { IconButton } from '.';
import { themeState } from '../globalStateAtoms';
import { useRecoilState } from 'recoil';
import { themes } from '../../theme';

export function ModeSwitchButton() {
  const [theme, setTheme] = useRecoilState(themeState);

  const changeMode = () => {
    theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);
  };

  return (
    <IconButton
      onClick={changeMode}
      className={`fas fa-${theme === themes.dark ? 'moon' : 'sun'}`}
      iconVariant="large"
    />
  );
}
