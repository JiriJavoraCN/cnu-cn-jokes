import React, { useState } from 'react';
import { IconButton } from '.';
import { startSpeech, stopSpeech } from '../utils';
import { Joke } from '../globalStateAtoms';

type Props = {
  joke: Joke;
};

export function TellJokeButton(props: Props) {
  const [isSpeaking, setIsSpeaking] = useState(false);
  const handleSpeechStart = () => {
    setIsSpeaking(true);
  };
  const handleSpeechStop = () => {
    setIsSpeaking(false);
  };

  const startSpeaking = () => {
    startSpeech(props.joke.value, handleSpeechStart, handleSpeechStop);
  };

  const stopSpeaking = () => {
    stopSpeech();
    setIsSpeaking(false);
  };

  return (
    <IconButton
      onClick={isSpeaking ? stopSpeaking : startSpeaking}
      iconVariant="onCard"
      buttonVariant="icon-button"
      className={isSpeaking ? 'fas fa-times' : 'fas fa-volume-up'}
      buttonRight="2em"
      boxBackground="transparent"
    />
  );
}
