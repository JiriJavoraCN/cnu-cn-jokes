import React from 'react';
import { IconButton } from '.';
import Link from 'next/link';
import { useRouter } from 'next/router';

export function AddJokeButton() {
  const router = useRouter();

  return (
    <Link href={router.pathname === '/add-jokes' ? '/' : '/add-jokes'} passHref>
      <IconButton
        className={`${router.pathname === '/add-jokes' ? 'fas fa-times' : 'fas fa-plus'}`}
        iconVariant="large"
      />
    </Link>
  );
}
