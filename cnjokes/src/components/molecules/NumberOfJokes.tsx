import React, { useState, useEffect, ChangeEvent } from 'react';
import { useSetRecoilState } from 'recoil';
import { numberOfJokesState } from '../globalStateAtoms';
import { Box, Icon, Input } from '../atoms';
import ReactTooltip from 'react-tooltip';

export function NumberOfJokes() {
  const [warning, setWarning] = useState('');
  const setNumberOfJokes = useSetRecoilState(numberOfJokesState);

  function handleInput(e: ChangeEvent<HTMLInputElement>) {
    const inputValidity = e.target.validity.valid;
    if (inputValidity) {
      setNumberOfJokes(Number(e.target.value));
      setWarning('');
    } else {
      setWarning(e.target.validationMessage);
    }
  }

  useEffect(() => {
    ReactTooltip.rebuild();
  }, [warning]);

  return (
    <>
      <Box padding="0" width="50%" minWidth="7em" display={'flex'} position="relative">
        <Input
          placeholder="# OF JOKES"
          type="number"
          min="1"
          max="50"
          step="1"
          onInput={handleInput}
          variant="small"
        />
        {!!warning && (
          <Icon variant="warning" className="fas fa-exclamation-triangle" data-tip={warning} />
        )}
      </Box>
    </>
  );
}
