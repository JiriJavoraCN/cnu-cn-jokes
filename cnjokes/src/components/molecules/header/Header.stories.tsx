import React from 'react';
import { Header } from './Header';

export default {
  title: 'molecules/Header',
  component: Header,
  argTypes: {
    text: {
      control: {
        type: 'text',
      },
      defaultValue: 'CN JOKES',
    },
  },
};

export const DefaultHeader = (args) => {
  return <Header text={args.text} />;
};
