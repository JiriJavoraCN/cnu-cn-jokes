import React from 'react';
import { Box, Heading } from '../../atoms';

type Props = {
  text?: string;
};
export function Header({ text }: Props) {
  return (
    <Box>
      <Heading variant="large">{text}</Heading>
    </Box>
  );
}
