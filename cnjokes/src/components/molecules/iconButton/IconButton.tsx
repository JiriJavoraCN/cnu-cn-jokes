import React from 'react';
import { Box, Button, Icon } from '../../atoms';
import { ButtonVariants } from '../../atoms/button/Button';
type Props = {
  flex?: string;
  boxBackground?: string;
  width?: string;
  height?: string;
  onClick?: () => void;
  buttonVariant?: ButtonVariants;
  buttonRight?: string;
  className?: string;
  iconVariant?: string;
  fontSize?: string;
  text?: string;
};

export function IconButton(props: Props) {
  return (
    <Box
      padding="0"
      flex={props.flex}
      backgroundColor={props.boxBackground}
      width={props.width}
      height={props.height}
    >
      <Button onClick={props.onClick} variant={props.buttonVariant} right={props.buttonRight}>
        {props.text}
        <Icon className={props.className} variant={props.iconVariant} fontSize={props.fontSize} />
      </Button>
    </Box>
  );
}
