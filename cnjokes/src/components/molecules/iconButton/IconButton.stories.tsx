import React, { useState } from 'react';
import { IconButton } from './IconButton';
import { Box, Button } from '../../atoms';

export default {
  title: 'molecules/IconButton',
  component: IconButton,
  argTypes: {
    size: {
      control: { type: 'range', min: 3, max: 10 },
      defaultValue: 3,
    },
  },
};

export const ModeSwitchButton = (args) => {
  const [mode, setMode] = useState('sun');
  return (
    <IconButton
      className={`fas fa-${mode}`}
      width={`${args.size}em`}
      height={`${args.size}em`}
      fontSize={`${args.size / 2}em`}
      onClick={() => (mode === 'sun' ? setMode('moon') : setMode('sun'))}
    />
  );
};
export const ShowFavoriteListButton = (args) => {
  const [isShowing, setIsShowing] = useState('r');
  return (
    <IconButton
      className={`fa${isShowing} fa-star`}
      width={`${args.size}em`}
      height={`${args.size}em`}
      fontSize={`${args.size / 2}em`}
      onClick={() => (isShowing === 'r' ? setIsShowing('s') : setIsShowing('r'))}
    />
  );
};

export const ShowCategoriesButton = (args) => {
  const [showingCategories, setShowingCategories] = useState(false);
  const categories = () => {
    const list: any = [];
    for (let i = 0; i < 10; i++) {
      list.push(<Button variant="category">THIS IS A CATEGORY</Button>);
    }
    return (
      <Box
        display="grid"
        padding="1em"
        margin="1em 0 0 0"
        visibility={showingCategories ? 'visible' : 'hidden'}
      >
        {list}
      </Box>
    );
  };
  const showCategories = () => {
    showingCategories === false ? setShowingCategories(true) : setShowingCategories(false);
  };
  return (
    <>
      <IconButton
        onClick={showCategories}
        text="CATEGORIES"
        className={`fas fa-${showingCategories ? 'caret-up' : 'caret-down'}`}
        iconVariant="small"
      />
      {categories()}
    </>
  );
};
