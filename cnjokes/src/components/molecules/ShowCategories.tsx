import React from 'react';
import { categoriesState } from '../globalStateAtoms';
import { IconButton } from '.';
import { useRecoilState } from 'recoil';

export function ShowCategories() {
  const [categories, setCategories] = useRecoilState(categoriesState);

  const showCategories = () => {
    setCategories((prevState) => {
      return { ...prevState, showCategories: !prevState.showCategories };
    });
  };

  return (
    <IconButton
      flex="40%"
      onClick={showCategories}
      text="CATEGORIES"
      className={`fas fa-${categories.showCategories ? 'caret-up' : 'caret-down'}`}
      buttonVariant="categories"
      iconVariant="small"
    />
  );
}
