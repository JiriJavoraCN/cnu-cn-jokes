import React, { ChangeEvent } from 'react';
import { SearchBar } from '.';
import { searchTextState } from '../globalStateAtoms';
import { useRecoilState } from 'recoil';

export function SearchJokeByText() {
  const [searchText, setSearchText] = useRecoilState(searchTextState);

  const handleSearchTextChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  return (
    <SearchBar
      placeholder="SEARCH JOKES..."
      onInput={handleSearchTextChange}
      value={searchText}
      inputVariant="large"
      className="fas fa-search"
    />
  );
}
