import { colorPalette } from './colorPalette';

export type Theme = {
  backgroundColor?: string;
  fontColor?: string;
  wrapperDivColor?: string;
  showJokesButtonColor?: string;
  showJokesButtonColorHovered?: string;
  placeholderTextColor?: string;
  optionSelectedColor?: string;
  optionSelectedHoveredColor?: string;
  favListButtonColor?: string;
  cardColor?: string;
  cardContentColor?: string;
  invalidInputColor?: string;
};

const dark: Theme = {
  backgroundColor: colorPalette.darkBackgroundColor,
  fontColor: colorPalette.black,
  wrapperDivColor: colorPalette.darkWrapperDivColor,
  showJokesButtonColor: colorPalette.darkShowJokesButtonBackground,
  showJokesButtonColorHovered: colorPalette.darkShowJokesButtonBackgroundHovered,
  placeholderTextColor: colorPalette.darkInputPlaceholderTextColor,
  optionSelectedColor: colorPalette.darkOptionSelectedColor,
  optionSelectedHoveredColor: colorPalette.darkOptionSelectedHoveredColor,
  favListButtonColor: colorPalette.darkActiveFavoriteListButton,
  cardColor: colorPalette.darkCardColor,
  cardContentColor: colorPalette.darkCardContentColor,
  invalidInputColor: colorPalette.darkInvalidInputColor,
};
const light: Theme = {
  backgroundColor: colorPalette.lightBackgroundColor,
  fontColor: colorPalette.white,
  wrapperDivColor: colorPalette.lightWrapperDivColor,
  showJokesButtonColor: colorPalette.lightShowJokesButtonBackground,
  showJokesButtonColorHovered: colorPalette.lightShowJokesButtonBackgroundHovered,
  placeholderTextColor: colorPalette.lightInputPlaceholderTextColor,
  optionSelectedColor: colorPalette.lightOptionSelectedColor,
  optionSelectedHoveredColor: colorPalette.lightOptionSelectedHoveredColor,
  favListButtonColor: colorPalette.lightActiveFavoriteListButton,
  cardColor: colorPalette.lightCardColor,
  cardContentColor: colorPalette.lightCardContentColor,
  invalidInputColor: colorPalette.lightInvalidInputColor,
};
export const themes = { dark, light };
