import facepaint from 'facepaint';

export const mq = facepaint([
  '@media(min-width: 625px)',
  '@media(min-width: 940px)',
  '@media(min-width: 1400px)',
]);
