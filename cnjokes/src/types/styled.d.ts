import { Theme } from '../theme/themes';
import 'styled-components';

declare module 'styled-components' {
  export type DefaultTheme = Theme;
}
