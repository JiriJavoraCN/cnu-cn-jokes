import { MongoClient } from 'mongodb';

const mongoDBuri = process.env.MONGODB_URI ?? '';

// @ts-ignore
let cached = global.mongo;

if (!cached) {
  // @ts-ignore
  cached = global.mongo = { conn: null, promise: null };
}

async function connectToDatabase(mongoDBuri: string) {
  if (cached.conn) {
    return cached.conn;
  }

  if (!cached.promise) {
    cached.promise = MongoClient.connect(mongoDBuri);
  }
  cached.conn = await cached.promise;
  return cached.conn;
}

export async function getCollection(collection: string) {
  await connectToDatabase(mongoDBuri);
  return cached.conn.db('customCNJokes').collection(collection);
}
