import { gql, ApolloServer } from 'apollo-server-micro';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import { NextApiRequest, NextApiResponse } from 'next';
import { getCollection } from '../../utils/mongoDBConnection';

const mongoDBjokesCollection = getCollection('allJokes');

const typeDefs = gql`
  type joke {
    value: String
    categories: [String]
    _id: String
  }

  type Query {
    getJokeCategories: [String]
    getRandomJokes(numOfJokes: Int!, jokeCategories: [String]): [joke]
    getJokesByText(text: String!, jokeCategories: [String]): [joke]
  }

  type Mutation {
    addCustomJoke(value: String!, categories: [String]): joke
  }
`;

interface joke {
  value: string;
  categories: string[];
  _id?: string;
}

const getCategoriesAggregationPipeline = [
  {
    $unwind: {
      path: '$categories',
    },
  },
  {
    $group: {
      _id: '$categories',
    },
  },
];

const createCategoryFilter = (categories: string[]) => {
  if (!categories?.length) return {};
  return { categories: { $in: categories } };
};

const resolvers = {
  Query: {
    getJokeCategories: async () => {
      return await (
        await mongoDBjokesCollection
      )
        .aggregate(getCategoriesAggregationPipeline)
        .toArray()
        .then((arr: joke[]) => arr.map((categoryItem) => categoryItem._id));
    },
    getRandomJokes: async (
      _: any,
      { numOfJokes, jokeCategories }: { numOfJokes: number; jokeCategories: string[] },
    ) => {
      return await (await mongoDBjokesCollection)
        .aggregate([
          { $match: createCategoryFilter(jokeCategories) },
          { $sample: { size: numOfJokes } },
        ])
        .toArray();
    },
    getJokesByText: async (
      _: any,
      { text, jokeCategories }: { text: string; jokeCategories: string[] },
    ) => {
      return await (await mongoDBjokesCollection)
        .aggregate([
          { $match: createCategoryFilter(jokeCategories) },
          { $match: { value: { $regex: text, $options: 'i' } } },
          { $limit: 25 },
        ])
        .toArray();
    },
  },
  Mutation: {
    addCustomJoke: async (
      _: any,
      { value, categories }: { value: string; categories: string[] },
    ) => {
      const joke = { value, categories };
      (await mongoDBjokesCollection).insertOne(joke);
      return joke;
    },
  },
};

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

const startServer = apolloServer.start();

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  await startServer;
  await apolloServer.createHandler({
    path: '/api/graphql',
  })(req, res);
}

export const config = {
  api: {
    bodyParser: false,
  },
};
