import React, { useState, useEffect } from 'react';
import { RecoilRoot } from 'recoil';
import ReactTooltip from 'react-tooltip';
import Head from 'next/head';
import { CNJokes } from '../src/components/CNJokes';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { AppProps } from 'next/app';

const client = new ApolloClient({
  uri: '/api/graphql',
  cache: new InMemoryCache(),
});

function App({ Component, pageProps }: AppProps) {
  const [SSR, SetSSR] = useState(true);

  useEffect(() => {
    SetSSR(false);
  }, []);

  return (
    <React.StrictMode>
      <Head>
        <title>CN Jokes</title>
      </Head>
      <RecoilRoot>
        <ApolloProvider client={client}>
          <CNJokes>
            <Component {...pageProps} />
          </CNJokes>
          {!SSR && (
            <>
              <ReactTooltip effect="solid" />
            </>
          )}
        </ApolloProvider>
      </RecoilRoot>
    </React.StrictMode>
  );
}

export default App;
