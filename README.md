# CN Jokes

Implementation of React application that consumes public API (part of CNU training).

- [API to consume](https://api.chucknorris.io/)

## Requirements

- App will display random CN joke
- App can display any number of jokes (count determined by the user)
- App will display all available categories of jokes
  - When clicked on a category, a random joke from this category will be displayed
  - User can input a number of jokes he wants to see in the current category, then jokes are displayed
- App will have input for full-text search of jokes, displaying the first 25 jokes returned from API as user types
- Suprise me 🙂

## Project info

- Wireframe - https://www.figma.com/file/cZEeNFrAzLI6lxIIm5gGMh/CN-Jokes?node-id=10%3A36
- Deployed on [vercel](https://cnu-cn-jokes.vercel.app/)
- Tech stack - Yarn + Next.js + Recoil + Styled components
- Honorable mentions - CSS in JS concept, TypeScript, Recoil vs Redux, Atomic Design, GraphQL, Storybook, Vercel deploy, git, team workflow, skill improvement

